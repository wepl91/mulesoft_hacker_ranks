// process.stdin.resume();
// process.stdin.setEncoding("ascii");
var input = "";
var directories = {
    '/': {
        'files': [],
        'directories': {}
    },
    '/home': {
        'files': [],
        'directories': {}
    }
}
var currentDirectory = {
  'path': '/',
  'node': directories['/']
}

var messages = {
    'existing_directory': 'Directory already exist',
    'nonexisting_directory': 'Directory not exist',
    'nonexiting_command': 'Command not exist',
    'long_name': 'File or directory name too long',
    'too_many_parameters': 'Command with too many parameters',
    'parameter_missing': 'Parameter is missing'
}


var commands = ['quit', 'ls', 'mkdir', 'touch', 'pwd', 'cd']

function changeDirectory(directoryName) {
  if (Object.keys(currentDirectory.node.directories).length < 1) {
    return showErrorMessage('nonexisting_directory')
  }
  Object.keys(currentDirectory.node.directories).forEach( (directory) => {
    if (directory != directoryName) {
      return showErrorMessage('nonexisting_directory')
    }
    else {
      currentDirectory['path'] = `${ currentDirectory['path'] }/${ directoryName }` 
      currentDirectory['node'] = currentDirectory['node'].directories[directoryName] 
    }
  })
}

function quitApplication() {
  return process.exit()
}

function listContent( parameter ) {
  currentDirectory.node.files.forEach( (file) => (
    process.stdout.write(file)
  ))

  Object.keys(currentDirectory.node.directories).forEach( (directory) => (
    process.stdout.write(directory)
  ))
}

function createDirectory( name ) {
  if (!name) return null
  
  currentDirectory.node.directories[name] = {
    'files':[],
    'directories':{}
  }
}

function createFile( name ) {
  if (!name) return null
  currentDirectory.node.files.push(name)
}

function showDirectory() {
  return process.stdout.write(currentDirectory.path);
}

function showErrorMessage(errorName) {
    return process.stdout.write(messages[errorName]);
}


function getCommand(string) {
    if (string.indexOf(' ') > 0) {
        if (string.split(' ').length > 2) {
            return showErrorMessage('too_many_parameters')
        }
        var command = string.split(' ')[0]
        var parameter = string.split(' ')[1]
        
        if (commands.indexOf(command) < 0) {
          return showErrorMessage('nonexiting_command')
        }

        if ((command == 'mkdir' || command == 'touch') && (!parameter || parameter.length < 1)) {
          return showErrorMessage('parameter_missing')
        }

        if (parameter && parameter.length > 100) {
          return showErrorMessage('long_name')
        }
        return getFunction(command, parameter)
    }
    else {
      if (commands.indexOf(string) < 0) {
        return showErrorMessage('nonexiting_command')
      }
      return getFunction(string, null)

    }
}

function getFunction( commandName, parameter ) {

  switch (commandName) {
    case 'pwd':
    return this.showDirectory()
    break
    case 'cd':
    return this.changeDirectory(parameter)
    break
    case 'quit':
    return this.quitApplication()
    break
    case 'touch':
    return this.createFile(parameter)
    break
    case 'ls':
    return this.listContent()
    break
    case 'mkdir':
    return this.createDirectory(parameter)
    break
  }
}


process.stdin.on("data", function ( chunk ) {
    getCommand(chunk)
    input += chunk;
});
process.stdin.on("end", function () {
    // now we can read/parse input
    process.exit()
});

